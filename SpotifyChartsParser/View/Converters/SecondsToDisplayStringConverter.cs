﻿using System;
using System.Windows.Data;

namespace SpotifyChartsParser.View.Converters
{
    [ValueConversion(typeof(int), typeof(string))]
    public class SecondsToDisplayStringConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int sec = (int)value;

            TimeSpan duration = TimeSpan.FromSeconds(sec);

            //here backslash is must to tell that colon is
            //not the part of format, it just a character that we want in output
            return duration.ToString(@"mm\:ss");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing; // Thanks to Danny Varod for the suggestion!
        }

        #endregion
    }
}
