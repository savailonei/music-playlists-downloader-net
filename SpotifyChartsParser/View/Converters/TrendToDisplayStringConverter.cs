﻿using SpotifyChartsParser.PlaylistDownloaders;
using System;
using System.Windows.Data;

namespace SpotifyChartsParser.View.Converters
{
    [ValueConversion(typeof(Trend), typeof(string))]
    public class TrendToDisplayStringConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Trend trend = (Trend)value;
            switch (trend)
            {
                case Trend.Down:
                    return "\u25BC"; // "down" triangle
                case Trend.NoChange:
                    return "-";
                case Trend.Up:
                    return "\u25B2"; // "up" triangle
                case Trend.Unknown:
                    return "";
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing; // Thanks to Danny Varod for the suggestion!
        }

        #endregion
    }
}
