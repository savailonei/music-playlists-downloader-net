﻿using System;
using System.Collections.Generic;

namespace SpotifyChartsParser.ViewModel
{
    public class FilterDefinition
    {
        public FilterDefinition(string name, string dataString)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            DataString = dataString ?? throw new ArgumentNullException(nameof(dataString));
        }

        public string Name { get; }
        public string DataString { get; }

        /*private static string dateFromat = "yyyy-MM-dd";

        // Generated enumerator for dates between two dates (starting from the latest)
        public static IEnumerable<DateTime> EachDay(DateTime date1, DateTime date2)
        {
            if(date1 < date2)
            {
                var tmp = date2;
                date2 = date1;
                date1 = tmp;
            }

            for (var day = date1.Date; day.Date >= date2.Date; day = day.AddDays(-1))
                yield return day;
        }

        private static List<FilterDefinition> knownDateFilters;

        public static List<FilterDefinition> KnownDateFilters
        {
            get
            {
                if(knownDateFilters == null)
                {
                    knownDateFilters = new List<FilterDefinition>();

                    foreach(var d in EachDay(DateTime.Now.AddMonths(-3), DateTime.Now.AddDays(-2)))
                    {
                        string dateString = d.ToString(dateFromat);
                        knownDateFilters.Add(new FilterDefinition(dateString, dateString));
                    }
                }

                return knownDateFilters;
            }
        }*/

        public override bool Equals(object obj)
        {
            return obj is FilterDefinition filter &&
                   DataString == filter.DataString;
        }

        public override int GetHashCode()
        {
            return -1406317640 + EqualityComparer<string>.Default.GetHashCode(DataString);
        }
    }
}
