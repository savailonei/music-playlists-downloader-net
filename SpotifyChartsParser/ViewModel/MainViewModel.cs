﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using SpotifyChartsParser.PlaylistDownloaders;
using SpotifyChartsParser.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpotifyChartsParser.ViewModel
{
    public class MainViewModel: NotifyPropertyChangedImplBase
    {
        public MainViewModel()
        {
            SongsList = new ObservableCollection<SongInfo>();
            var websites = new List<WebsiteDefinition>();
            websites.Add(new WebsiteDefinition("spotifycharts.com", new DownloaderBase()));
            WebsiteDefinitions = websites;

            SelectedWebsite = WebsiteDefinitions[0];
        }

        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        private bool _isDownloadPlaylistsRuning;
        private RelayCommand downloadPlaylistsCommand;

        public RelayCommand DownloadPlaylistsCommand
        {
            get
            {
                return downloadPlaylistsCommand
                  ?? (downloadPlaylistsCommand = new RelayCommand(
                    async () =>
                    {
                        if (_isDownloadPlaylistsRuning)
                        {
                            return;
                        }

                        _isDownloadPlaylistsRuning = true;
                        downloadPlaylistsCommand.RaiseCanExecuteChanged();

                        if (SelectedWebsite != null)
                        {
                            SongsList.Clear();

                            IList<SongInfo> songsList = await Task.Run(() =>
                                {
                                    try
                                    {
                                        SelectedWebsite.Downloader.SongInfoAvailable += Downloader_SongInfoAvailable;

                                        var songs = SelectedWebsite.Downloader.Parse(SelectedBaseUri.DataString, SelectedRegionFilter, SelectedReccurenceFilter, SelectedDateFilter,
                                                    new Progress<double>((p) => Progress = p),
                                                    (m) => Message = m,
                                                    cancellationTokenSource.Token
                                                );

                                        return songs;
                                    }
                                    finally
                                    {
                                        SelectedWebsite.Downloader.SongInfoAvailable -= Downloader_SongInfoAvailable;
                                    }
                                }, cancellationTokenSource.Token
                            );

                            /*foreach (var song in songsList)
                            {
                                SongsList.Add(song);
                            }*/
                        }
                        else
                        {
                            Message = "Please select website first";
                        }

                        _isDownloadPlaylistsRuning = false;
                        downloadPlaylistsCommand.RaiseCanExecuteChanged();
                    },
                    () => !_isDownloadPlaylistsRuning));
            }
        }

        private void Downloader_SongInfoAvailable(object sender, SongInfo songInfo)
        {
            lock (SongsList)
            {
                int pos = 0;
                foreach(var s in SongsList)
                {
                    if(songInfo.Position > pos)
                    {
                        pos += 1;
                    }
                }

                App.Current.Dispatcher.Invoke(delegate
                {
                    SongsList.Insert(pos, songInfo);
                }); 
            }
        }

        private List<WebsiteDefinition> websiteDefinitions;
        public List<WebsiteDefinition> WebsiteDefinitions
        {
            get => websiteDefinitions;
            //private set => SetProperty(ref websiteDefinitions, value);
            private set => websiteDefinitions = value;
        }

        private List<FilterDefinition> baseUris;
        public List<FilterDefinition> BaseUris
        {
            get => baseUris;
            set => SetProperty(ref baseUris, value);
        }

        private FilterDefinition selectedBaseUri;
        public FilterDefinition SelectedBaseUri
        {
            get => selectedBaseUri;
            set => SetProperty(ref selectedBaseUri, value);
        }

        private WebsiteDefinition selectedWebsite;
        public WebsiteDefinition SelectedWebsite
        {
            get => selectedWebsite;
            set
            {
                var filterDefinitions = value.Downloader.DownloadFilterDefinitions(new Progress<double>((p) => Progress = p), (m) => Message = m, cancellationTokenSource.Token);

                RegionsFilters = filterDefinitions["country"];
                ReccurenceFilters = filterDefinitions["recurrence"];
                DateFilters = filterDefinitions["date"];
                BaseUris = value.Downloader.BaseUris;

                SelectedRegionFilter = RegionsFilters[0];
                SelectedReccurenceFilter = ReccurenceFilters[0];
                SelectedDateFilter = DateFilters[0];
                SelectedBaseUri = BaseUris[0];

                SetProperty(ref selectedWebsite, value);
            }
        }

        public ObservableCollection<SongInfo> SongsList { get;  }

        private List<FilterDefinition> regionsFilters;
        public List<FilterDefinition> RegionsFilters
        {
            get => regionsFilters;
            set => SetProperty(ref regionsFilters, value);
        }

        private FilterDefinition selectedRegionFilter;
        public FilterDefinition SelectedRegionFilter
        {
            get => selectedRegionFilter;
            set => SetProperty(ref selectedRegionFilter, value);
        }

        private List<FilterDefinition> reccurenceFilters;
        public List<FilterDefinition> ReccurenceFilters
        {
            get => reccurenceFilters;
            set => SetProperty(ref reccurenceFilters, value);
        }

        private FilterDefinition selectedReccurenceFilter;
        public FilterDefinition SelectedReccurenceFilter
        {
            get => selectedReccurenceFilter;
            set => SetProperty(ref selectedReccurenceFilter, value);
        }

        private List<FilterDefinition> dateFilters;
        public List<FilterDefinition> DateFilters
        {
            get => dateFilters;
            set => SetProperty(ref dateFilters, value);
        }

        private FilterDefinition selectedDateFilter;
        public FilterDefinition SelectedDateFilter
        {
            get => selectedDateFilter;
            set => SetProperty(ref selectedDateFilter, value);
        }

        private string message;
        public string Message
        {
            get => message;
            set => SetProperty(ref message, value);
        }

        public double progress;
        public double Progress
        {
            get => progress;
            set => SetProperty(ref progress, value);
        }
    }
}
