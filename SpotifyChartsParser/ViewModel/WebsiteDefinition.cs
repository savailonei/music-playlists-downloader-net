﻿using SpotifyChartsParser.PlaylistDownloaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyChartsParser.ViewModel
{
    public class WebsiteDefinition
    {
        public WebsiteDefinition(string name, DownloaderBase downloader)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Downloader = downloader ?? throw new ArgumentNullException(nameof(downloader));
        }

        public string Name { get; set; }
        public DownloaderBase Downloader { get; set; }


    }
}
