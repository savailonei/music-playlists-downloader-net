﻿using System;
using System.Windows.Media.Imaging;

namespace SpotifyChartsParser.PlaylistDownloaders
{
    public enum Trend
    {
        Unknown, Down, NoChange, Up
    }
    public class SongInfo
    {
        public SongInfo(string artist, string name, int lengthSec, string url, BitmapImage albumCover, Trend trend, int position)
        {
            Artist = artist ?? throw new ArgumentNullException(nameof(artist));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            LengthSec = lengthSec;
            Url = url ?? throw new ArgumentNullException(nameof(url));
            AlbumCover = albumCover;
            Trend = trend;
            Position = position;
        }

        public BitmapImage AlbumCover { get; }
        public string Artist { get; }
        public string Name { get; }
        public int LengthSec { get; }
        public string Url { get; }

        public Trend Trend { get; }

        /// <summary>
        /// Zero-based position in a chart.
        /// </summary>
        public int Position { get; }
    }
}
