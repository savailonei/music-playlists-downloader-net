﻿using HtmlAgilityPack;
using SpotifyChartsParser.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace SpotifyChartsParser.PlaylistDownloaders
{

    public class DownloaderBase
    {
        private static HttpClient Client = new HttpClient();

        public List<FilterDefinition> BaseUris { get; set; } = new List<FilterDefinition>() {
            new FilterDefinition("Top 200", "https://spotifycharts.com/regional/"),
            new FilterDefinition("Viral 50", "https://spotifycharts.com/viral/"),
        };


        public Task<string> Download(Uri url, int sequenceNumber = -1)
        {
            return Client.GetStringAsync(url);
            // Console.WriteLine(result.StatusCode);
        }

        public BitmapImage DownloadImage(string url, int sequenceNumber = -1)
        {
            //we need this in memory to be able to freeze the image (and be ale to bind to it from XAML without the "Dependecy source needs to be created in the same thread etc...")
            MemoryStream tmpStream = new MemoryStream();
            using (var stream = Client.GetStreamAsync(url).Result)
            {
                stream.CopyTo(tmpStream);
                tmpStream.Seek(0, SeekOrigin.Begin);
            }

            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = tmpStream;
            image.EndInit();

            image.Freeze();
            return image;
        }
        // /regional/global/daily/latest
        private static string spotifyUrlPathTemplate = "{0}/{1}/{2}";

        public Dictionary<string, List<FilterDefinition>> DownloadFilterDefinitions(IProgress<double> progress, Action<string> statusMessage, CancellationToken cancellationToken)
        {
            if(BaseUris.Count == 0)
            {
                return new Dictionary<string, List<FilterDefinition>>(0);
            }

            string html = Download(new Uri(BaseUris.First().DataString)).Result;
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            if (htmlDoc.ParseErrors != null && htmlDoc.ParseErrors.Count() > 0)
            {
                // Handle any parse errors as required
                statusMessage("Error occured: " + htmlDoc.ParseErrors.First().Reason);
            }

            var regionFilters = new List<FilterDefinition>();
            var reccurenceFilters = new List<FilterDefinition>();
            var dateFilters = new List<FilterDefinition>();

            HtmlNodeCollection filtersListNodes = htmlDoc.DocumentNode.SelectNodes(".//div[@class='chart-filters-list'][1]/div");

            var filterDefinitions = new Dictionary<string, List<FilterDefinition>>(filtersListNodes.Count);

            foreach (var filterListNode in filtersListNodes)
            {
                string filterName = filterListNode.GetAttributeValue("data-type", null);

                HtmlNodeCollection filterElemNodes = filterListNode.SelectNodes("./ul[1]/li");

                var filterValues = new List<FilterDefinition>(filterElemNodes.Count);
                foreach (var filterElem in filterElemNodes)
                {
                    string dataString = filterElem.GetAttributeValue("data-value", null);
                    string name = filterElem.InnerText.Trim();

                    filterValues.Add(new FilterDefinition(name, dataString));
                }

                filterDefinitions.Add(filterName, filterValues);
            }

            return filterDefinitions;
        }

        private Trend ExtractTrend(HtmlNode svgNode)
        {
            if (svgNode != null)
            {
                var polygonNode = svgNode.SelectSingleNode(".//polygon");
                // If there is no "polygon" we assume rectangle is there which denotes "unchanged"
                if(polygonNode == null)
                {
                    return Trend.NoChange;
                }
                else
                {
                    var pointsStr = polygonNode.GetAttributeValue("points", null);
                    if(pointsStr != null)
                    {
                        // Triangle pointing up?
                        if (pointsStr.Trim().StartsWith("0"))
                        {
                            return Trend.Up;
                        }
                        else
                        {
                            return Trend.Down;
                        }
                    }
                }
            }

            return Trend.Unknown;
        }

        private class SongUrlInfo1
        {
            public readonly string songPageUrl;
            public readonly Trend trend;
            public readonly string imageUrl;

            public SongUrlInfo1(string songPageUrl, Trend trend, string imageUrl)
            {
                this.songPageUrl = songPageUrl;
                this.trend = trend;
                this.imageUrl = imageUrl;
            }
        }

        public delegate void SongInfoAvailableEventHandler(object sender, SongInfo songInfo);
        public event SongInfoAvailableEventHandler SongInfoAvailable;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="songInfo">The song info</param>
        /// <param name="position">Zero-based position of the song in a chart.</param>
        protected void OnSongInfoAvailable(SongInfo songInfo)
        {
            SongInfoAvailable?.Invoke(this, songInfo);
        }

        public IList<SongInfo> Parse(string baseUriStr, FilterDefinition regionFilter,FilterDefinition reccurenceFilter, FilterDefinition dateFilter,
            IProgress<double> progress, Action<string> statusMessage, CancellationToken cancellationToken)
        {
            try
            {
                Uri baseUri = new Uri(baseUriStr);

                string urlPath = string.Format(spotifyUrlPathTemplate, regionFilter.DataString, reccurenceFilter.DataString, dateFilter.DataString);

                //Uri baseUri = new Uri("https://spotifycharts.com");
                Uri chartsUri = new Uri(baseUri, urlPath);

                statusMessage("Processing: " + baseUri.ToString());
                progress.Report(1);

                
                string html = Download(chartsUri).Result;

                HtmlDocument htmlDoc = new HtmlDocument();

                // There are various options, set as needed
                //htmlDoc.OptionFixNestedTags = true;

                // filePath is a path to a file containing the html
                htmlDoc.LoadHtml(html);

                // ParseErrors is an ArrayList containing any errors from the Load statement
                if (htmlDoc.ParseErrors != null && htmlDoc.ParseErrors.Count() > 0)
                {
                    // Handle any parse errors as required
                    statusMessage("Error occured: " + htmlDoc.ParseErrors.First().Reason);
                }
                else
                {

                    statusMessage("Processing songs...");

                    if (htmlDoc.DocumentNode != null)
                    {
                        HtmlNode bodyNode = htmlDoc.DocumentNode.SelectSingleNode(".//body");

                        if (bodyNode != null)
                        {
                            // Do something with bodyNode
                        }

                        var chartTableRecords = bodyNode.SelectNodes(@".//*[@id='content']/div/div/div//table//tr");

                        var songUrlInfos = chartTableRecords.Select(row =>
                        {
                            var songPageLinkNode = row.SelectSingleNode("./td[1]/a");

                            if (songPageLinkNode == null)
                            {
                                return null;
                            }

                            string songPageUrl = songPageLinkNode.GetAttributeValue("href", null);

                            var trendNode = row.SelectSingleNode("./td[@class='chart-table-trend']//svg");

                            Trend trend = ExtractTrend(trendNode);

                            //get image url
                            HtmlNode imageNode = songPageLinkNode.SelectSingleNode("./img");
                            string imageUrl = null;

                            if (imageNode != null)
                            {
                                imageUrl = imageNode.GetAttributeValue("src", null);
                            }

                            return new SongUrlInfo1(songPageUrl, trend, imageUrl);
                        }).Where(info => info != null).ToArray();

                        // float as we need floating point calculations when this variable is used
                        float numSongPages = songUrlInfos.Length;
                        int pageNum = 0;
                        SongInfo[] songInfos = new SongInfo[songUrlInfos.Length];

                        ParallelOptions parallelOptions1 = new ParallelOptions();
                        parallelOptions1.CancellationToken = cancellationToken;

                        Parallel.For(0, songUrlInfos.Length, parallelOptions1, (int index, ParallelLoopState state) =>
                        {
                            var songUrlInfo = songUrlInfos[index];

                            string songPageHtml = Download(new Uri(songUrlInfo.songPageUrl)).Result;

                            BitmapImage image = null;
                            //get image
                            if (songUrlInfo.imageUrl != null)
                            {
                                image = DownloadImage(songUrlInfo.imageUrl);
                            }

                            HtmlDocument songPageHtmlDoc = new HtmlDocument();
                            songPageHtmlDoc.LoadHtml(songPageHtml);

                            var headElem = songPageHtmlDoc.DocumentNode.SelectSingleNode(".//head");

                            var artistNodes = songPageHtmlDoc.DocumentNode
                                .SelectNodes("(.//*[@id='main-header'])[1]/following-sibling::div[1]/div[1]/div[@class='media-bd']/h2/a");

                            List<string> artists = new List<string>(artistNodes.Count);

                            foreach (var artistNode in artistNodes)
                            {
                                artists.Add(artistNode.InnerText.Trim());
                            }

                            string songName = headElem.SelectSingleNode("./meta[@property='og:title']").GetAttributeValue("content", "");
                            songName = HtmlEntity.DeEntitize(songName);

                            string songDurationStr = headElem.SelectSingleNode("./meta[@property='music:duration']").GetAttributeValue("content", "");
                            int songDuration = int.Parse(songDurationStr);

                            SongInfo songInfo = null;

                            lock (songInfos)
                            {
                                songInfo = new SongInfo(string.Join(", ", artists.ToArray()), songName,
                                    songDuration, songUrlInfo.songPageUrl, image, songUrlInfo.trend, index);

                                songInfos[index] = songInfo;
                            }

                            OnSongInfoAvailable(songInfo);

                            lock (progress)
                            {
                                pageNum += 1;
                                progress.Report((pageNum / numSongPages) * 100);
                            }
                        });

                        return new List<SongInfo>(songInfos);
                    }
                }
            }
            catch (Exception ex)
            {
                progress.Report(0);
                statusMessage("Error occured: " + ex.Message);
            }

            return new List<SongInfo>(0);
        }
    }
}
